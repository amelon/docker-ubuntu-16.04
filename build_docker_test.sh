#!/bin/sh

id=2
name=ubuntu-16.04
image=amelon1/${name}

docker build \
    -t $image:$id \
    -t $image:latest \
    -f Dockerfile \
    .

