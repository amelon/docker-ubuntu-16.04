#!/bin/sh
# When pushing out an image to DockerHub without specifying the tag, it will look for the latest
# and the tag ID that it is associated with.
name=ubuntu-16.04
image=amelon1/${name}
docker push $image

