FROM ubuntu:16.04
MAINTAINER Vui Le "amelon@gmail.com"


#------------------------------------------------------------------------------
# Core tools
#------------------------------------------------------------------------------

RUN apt-get update \
    && apt-get install -y \
              vim git \
              software-properties-common \
              python python-dev python-setuptools python-pip \
              python-software-properties python-yaml python-tk \
              curl \
              language-pack-en-base

RUN pip install --upgrade pip

#------------------------------------------------------------------------------
# Miscellaneous
#------------------------------------------------------------------------------

# Adjust timezone from UTC to PST
RUN mv /etc/localtime /etc/localtime.old \
    && ln -s /usr/share/zoneinfo/America/Los_Angeles /etc/localtime

RUN mkdir /code
COPY code/entrypoint_loop.sh /code
